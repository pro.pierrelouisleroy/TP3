document.getElementById("envoyer").addEventListener("click", function(event){
    event.preventDefault();
    var user = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var req = new XMLHttpRequest();
	req.open('POST', '/login?user='+user+'&password='+password, true);

	req.onreadystatechange = function (aEvt) {
	  if (req.readyState == 4) {
	     if(req.status == 200)
	     {
	     	//console.log(req.responseText);
	     	document.getElementById("message").innerHTML = req.responseText;
	     }
	     else
	     {
	      console.log("Erreur pendant le chargement de la page.\n");	     	
	     }
	  }
	};
	req.send(null);
});