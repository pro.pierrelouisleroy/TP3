// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var crypto = require('crypto');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/checksession", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

app.get("/deletesession", function(req, res, next) {
	db.run('DELETE FROM sessions;', function(err, data) {
		res.json({delete:"ok"});
	});
});

app.post("/login", function(req, res, next) {

	var username = req.query.user;
	var password = req.query.password;

	db.get('SELECT count(*) AS nbuser FROM users WHERE ident =? and password =?;',[username,password] ,function(err, data) {
	/*		res.send(data);
			console.log(data);*/
		var status = data.nbuser == 1?true:false;
		if(status == true)
		{
			var token = crypto.randomBytes(64).toString('hex');
			res.cookie('infos_cookie', {status:status,token:token});			
			res.json({status:status,token:token});
			db.run('INSERT INTO sessions (token) VALUES (?)', [token]);
			//console.log(token);
		}
	});

});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
